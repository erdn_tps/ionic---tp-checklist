<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PreflightController extends ApiController
{
    public function main()
    {
        return response()->json( ['success' => true] )
            ->header('Access-Control-Allow-Methods', '*')
            ->header('Access-Control-Allow-Headers', 'Content-type');
    }
}

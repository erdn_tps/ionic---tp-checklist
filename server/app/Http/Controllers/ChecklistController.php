<?php

namespace App\Http\Controllers;

use App\User;
use App\Checklist;
use Illuminate\Http\Request;

class ChecklistController extends ApiController
{

    public function index()
    {
        return response()->json( Checklist::all() );
    }

    public function store(Request $request)
    {
        $checklist = Checklist::create([
            'title' => $request->title,
            'user_id' => $request->user_id
        ]);

        return response()->json( $checklist );
    }

    public function show(Checklist $checklist)
    {
        $checklist->items;
        return response()->json( $checklist );
    }

    public function update(Checklist $checklist, Request $request)
    {
        $fields = $request->validate([
            'title' => 'required'
        ]);
        $checklist->update($fields);
        return response()->json( $checklist );
    }

    public function destroy(Checklist $checklist)
    {
        $checklist->delete();
        return response()->json( $checklist );
    }

    public function showForUser( User $user )
    {
        $checklist = Checklist::with('items')->where('user_id', $user->id)->get();
        return response()->json( $checklist );
    }
}

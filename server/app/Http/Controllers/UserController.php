<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends ApiController
{

    public function show( User $user )
    {
        $user->checklists;
        return response()->json( $user );
    }

    public function auth( Request $request ) 
    {
        $user = User::where('username', $request->username)->first();
        if( $user && Hash::check($request->password, $user->password) ) {
            return response()->json($user);
        }
        else {
            return response()->json(['error' => 'Identifiants incorrects']);
        }
    }
    
}

<?php

namespace App\Http\Controllers;

use App\Item;
use App\Checklist;
use Illuminate\Http\Request;

class ItemController extends ApiController
{

    public function index()
    {
        return response()->json( Item::all() );
    }

    public function store(Request $request)
    {
        $item = Item::create([
            'label' => $request->label,
            'checked' => $request->checked,
            'checklist_id' => $request->checklist_id
        ]);

        return response()->json( $item );
    }

    public function update(Request $request, Item $item)
    {
        $fields = $request->validate([
            'label' => 'required',
            
        ]);
        $item->update($fields);
        return response()->json( $item );
    }

    public function updateCheck(Request $request, Item $item)
    {
        $fields = $request->validate([
            'checked' => 'required'
        ]);
        $item->update($fields);
        return response()->json( $item );
    }

    public function destroyChecked(Checklist $checklist)
    {                      
        $checklist->items()->where('checked',  1)->delete();
        $checklist->items;

        return response()->json($checklist);
    }
}

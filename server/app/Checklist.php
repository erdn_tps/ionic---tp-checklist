<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $fillable = [
        'title', 'user_id'
    ];

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}

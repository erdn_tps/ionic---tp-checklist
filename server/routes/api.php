<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::options('{any?}', 'PreflightController@main')->where('any', '.*');

Route::resource('users', 'UserController')
    ->only(['show']);

Route::resource('checklists', 'ChecklistController')
    ->except(['create', 'edit']);

Route::resource('items', 'ItemController')
    ->except(['create', 'show', 'edit', 'destroy']);

Route::post('/auth', 'UserController@auth');

Route::get('/checklists/user/{user}', 'ChecklistController@showForUser');

Route::put('/items/{item}/state', 'ItemController@updateCheck');

Route::delete('/checklists/items/{checklist}', 'ItemController@destroyChecked');
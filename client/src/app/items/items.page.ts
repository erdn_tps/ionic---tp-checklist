import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ChecklistService } from '../services/checklist.service';
import { AuthService } from '../services/auth.service';
import { Checklist } from '../models/checklist';
import { ItemService } from '../services/item.service';
import { Item } from '../models/item';

@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage {

    public display_input: boolean;

    public checklist: Checklist = new Checklist;

    public item_label: string = '';

  constructor( private checklistService: ChecklistService,
                private itemService: ItemService,
                private authService: AuthService,
                private activatedRoute: ActivatedRoute,
                private router: Router ) { }

    ionViewDidEnter() {
        this.activatedRoute
        .paramMap
        .subscribe( param => {
            this.checklist.id = parseInt( param.get('id') );
            if( this.checklist.id > 0 ) {
                this.getList();
            }
        });
    }

    // on retrouve tout les items correspondant à la CL demandée
    async getList() {
      this.checklist = await this.itemService.allForChecklist( this.checklist.id );
    }


    // on attend l'action de l'utilisateur pour savoir s'il faut afficher les items ou créer une CL
    async saveChecklist(id: number) {
        if ( this.checklist.id == 0 ) {
            this.createChecklist();
        }
        else { 
            this.updateChecklist( id ); 
        }
    }

    // méthode pour créer une CL
    async createChecklist() {
        // on appelle la methode du service en lui passant le title et l'id de l'utilisateur
        await this.checklistService.createChecklist(this.checklist.title, this.authService.user.id);
        // on redirige vers la CL qui vint d'être créée
        this.router.navigateByUrl(`/checklists/${this.checklistService.newChecklist_id}`);
    }

    // méthode pour update le titre d'une CL
    async updateChecklist( checklist_id ) {
        // on appelle la methode du service en lui passant l'id et le titre de la CL 
        await this.checklistService.updateChecklist( checklist_id, this.checklist.title.trim() );
    }

    showInput() {
        this.display_input = true;
    }

    hideInput() {
        this.display_input = false;
    }

    // méthode pour créer un item
    async createItem() {
        const checked = false;

        // on appelle la methode du service en lui passant le label de l'item, l'état de la CB et l'id de la CL 
        const newItem = await this.itemService.createItem(this.item_label, checked, this.checklist.id);
        // on push ce nouvel item dans le tableau de tout les item pour la mise à jour visuelle
        this.checklist.items.push(newItem);
        this.hideInput();
        this.item_label = '';
    }

    // methode pour update le titre d'un item
    async updateItem( item: Item ) {
        await this.itemService.updateItem( item.id, item.label.trim() );
    }

    async updateCheck( item: Item, $event ) {
        item.checked = $event.detail.checked;
        await this.itemService.updateState( item.id, item.checked );
    }

    async destroyCheckedItems( checklist: Checklist ) {
        await this.itemService.destroyChecked( checklist.id );
        this.checklist.items = this.itemService.items;
    }

}

import { Component } from '@angular/core';
import { ChecklistService } from '../services/checklist.service';
import { Checklist } from '../models/checklist';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-checklists',
  templateUrl: './checklists.page.html',
  styleUrls: ['./checklists.page.scss'],
})
export class ChecklistsPage {

    public checklists: Checklist[] = [];

  constructor( private checklistService: ChecklistService,
        private authService: AuthService ) { }

  ionViewDidEnter() {
      this.getList();
  }

  // on retrouve toutes les checklists correspondant à l'utilisateur connecté
  async getList() {
    this.checklists = await this.checklistService.allForUser( this.authService.user.id );
  }

  // la methode attend la demade de suppression de la checklist correspondante
  delete( checklist ) {
     // une fois le demande reçue, on appelle la methode du service en lui donnant l'id de la CL concernée
     this.checklistService.destroyChecklist( checklist.id );
     this.remove( checklist );
  }

  // methode pour retirer la cl du tableau de rendu
  remove( checklist ) {
    this.checklists.splice( this.checklists.indexOf(checklist), this.authService.user.id );
 }

}

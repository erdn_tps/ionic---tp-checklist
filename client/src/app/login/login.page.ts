import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

    public username: string = '';
    public password: string = '';

  constructor( private authService: AuthService,
    private router: Router ) { }

  login() {
      // on retire les espaces en début et fin de champ
      this.username = this.username.trim();
      this.password = this.password.trim();

      this.authService.auth({ username: this.username, password: this.password } as User)
        .then( responseUser => {
            if(responseUser.success) this.router.navigateByUrl('/checklists');
            else console.log( responseUser.error );
        });
}
}

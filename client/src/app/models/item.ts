export class Item {
    public id: number;
    public label: string;
    public checked: boolean;
    public checklist_id: number;

    constructor( itemLitteral: ItemLitteral ) {
       
        this.id = itemLitteral.id;
        this.label = itemLitteral.label;
        this.checked = parseInt(itemLitteral.checked) ? true : false;
        this.checklist_id = itemLitteral.checklist_id;
    }
}

export interface ItemLitteral {
    id: number;
    label: string;
    checked: string;
    checklist_id: number;
}


import { ItemLitteral, Item } from './item';

export class Checklist {
    public id: number;
    public title: string;
    public user_id: number;
    public items: any[] = [];

    constructor( checklistLitteral?: ChecklistLitteral ) {
        if(checklistLitteral){
            this.id = checklistLitteral.id;
            this.title = checklistLitteral.title;
            this.user_id = checklistLitteral.user_id;

            if( checklistLitteral.items ) {
                this.items = checklistLitteral.items;
            }
        }
    }
}

export interface ChecklistLitteral {
    id: number;
    title: string;
    user_id: number;

    items?: ItemLitteral[];
}

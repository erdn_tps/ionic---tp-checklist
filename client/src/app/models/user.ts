import { ChecklistLitteral } from './checklist';

export class User {
    public id: number;
    public username: string;
    public password: string;

    constructor( userLitteral: UserLitteral ) {
        this.id = userLitteral.id;
        this.username = userLitteral.username;
        this.password = userLitteral.password;
    }
}

export interface UserLitteral {
    id: number;
    username: string;
    password: string;

    checklists?: ChecklistLitteral[];
}

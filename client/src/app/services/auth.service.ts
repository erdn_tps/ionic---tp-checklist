import { Injectable } from '@angular/core';
import { User } from '../models/user';

export interface ResponseUser {
    success: boolean,
    user?: User,
    error?: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    public user: User = null;

  constructor() { }

  public auth(user: User) {
      const formData = new FormData();

      formData.append('username', user.username);
      formData.append('password', user.password);

      return fetch(`http://127.0.0.1:8000/api/auth`, { method: 'POST', body: formData })
        .then( response => response.json() )
        .then( user => {
            if(user.id) {
                this.user = user;
                return { success: true, user: user };
            }
            else return { success: false, error: 'No user found' }
        });

  }
}

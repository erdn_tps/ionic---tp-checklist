import { Injectable } from '@angular/core';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

    public newChecklist_id: number;

  constructor() { }

  // méthode faisant appel à l'api pour retrouver toute les cl correspondant à l'id user envoyé
  allForUser( id: number ) {
    return fetch(`http://127.0.0.1:8000/api/checklists/user/${id}`)
        .then( response => response.json() )
        .then( data => {
            return data;
        });
  }

  // méthode pour créer une CL
  createChecklist( title: string, user_id: number ) {

    const formData: FormData = new FormData();

    formData.append('title', title);
    formData.append('user_id', user_id.toString());
    
    return fetch(`http://127.0.0.1:8000/api/checklists`, { method: 'POST', body: formData })
        .then( response => response.json() )
        .then( data => {
            // on enregistre l'id de la CL qui vient d'être créée pour facilité la redirection
            this.newChecklist_id = data.id;
            return data;
        });
  }

  // méthode pour update une CL
  updateChecklist( checklist_id: number, new_title: string ) {

    // une méthode update demande un header spécial pour fonctionner
    const myHeaders = new Headers({
        'Content-type': 'application/json'
    });

    // en PUT, il faut envoyer du JSON pour que la méthode fonctionne
    const checklist_update = { "title" : new_title };
    return fetch(`http://127.0.0.1:8000/api/checklists/${checklist_id}`, 
        { 
            method: 'PUT',
            body: JSON.stringify(checklist_update),
            headers: myHeaders    
        })
        .then( response => response.json() )
        .then( data => {
            return data;
        });

  }

  // méthode pour supprimer une CL
  destroyChecklist( id: number ) {
    return fetch(`http://127.0.0.1:8000/api/checklists/${id}`, { method: 'DELETE' })
    .then( response => response.json() )
    .then( data => {
        return data;
    });
  }

}

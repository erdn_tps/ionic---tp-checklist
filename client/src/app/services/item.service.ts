import { Injectable } from '@angular/core';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

    public items: Item[] = [];

  constructor() { }

  
  allForChecklist( id: number ) {
    return fetch(`http://127.0.0.1:8000/api/checklists/${id}`)
        .then( response => response.json() )
        .then( data => {
            return data;
        });
  }

  createItem( label: string, checked: boolean, checklist_id: number ) {
    const formData: FormData = new FormData();

    formData.append('label', label);
    formData.append('checked', checked ? '1' : '0');
    formData.append('checklist_id', checklist_id.toString());
    
    return fetch(`http://127.0.0.1:8000/api/items`, { method: 'POST', body: formData })
        .then( response => response.json() )
        .then( data => {
            return new Item(data);
        });
  }

  updateItem( item_id: number, new_label: string ) {

    const myHeaders = new Headers({
        'Content-type': 'application/json'
    });

    const item_update = { "label" : new_label };
    return fetch(`http://127.0.0.1:8000/api/items/${item_id}`, 
        { 
            method: 'PUT',
            body: JSON.stringify(item_update),
            headers: myHeaders    
        })
        .then( response => response.json() )
        .then( data => {
            return data;
        });

  }

  updateState( item_id: number, new_state: boolean ) {
    const myHeaders = new Headers({
        'Content-type': 'application/json'
    });

    const item_update = { "checked" : new_state };
    return fetch(`http://127.0.0.1:8000/api/items/${item_id}/state`, 
        { 
            method: 'PUT',
            body: JSON.stringify(item_update),
            headers: myHeaders    
        })
        .then( response => response.json() )
        .then( data => {
            return data;
        });
  }

  destroyChecked( checklist_id: number ) {
    return fetch(`http://127.0.0.1:8000/api/checklists/items/${checklist_id}`, { method: 'DELETE' })
    .then( response => response.json() )
    .then( data => {
        this.items = data.items;
        return data;
    });
  }
}
